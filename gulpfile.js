const gulp = require('gulp');
   gulpLoadPlugins = require('gulp-load-plugins'),
   plugins = gulpLoadPlugins();
   browserSync = require('browser-sync');
   browserSync = require("browser-sync").create();


gulp.task('watch', ['sass'], function() {

   browserSync.init({
       server: "./"
   });

   gulp.watch("sass/*.sass", ['sass']);
   gulp.watch("*.html").on('change', browserSync.reload);
});

gulp.task('sass', function() {
   return gulp.src("sass/*.sass")
       .pipe(plugins.sass())
       .pipe(gulp.dest("css"))
       .pipe(browserSync.stream());
});

gulp.task('default', ['watch']);

gulp.task('build', ['sass'], function() {

    var buildCss = gulp.src([ // Переносим библиотеки в продакшен
        'css/main.css'
        ])
    .pipe(gulp.dest('dist/css'))

    var buildImages = gulp.src([ // Переносим картинки в продакшен
        'images/*.png',
        'images/*.jpg'
    ])
    .pipe(gulp.dest('dist/images'))

    var buildVegas = gulp.src([
        'vegas/**/*.*'
    ])
    .pipe(gulp.dest('dist/vegas'))

    var buildHtml = gulp.src('*.html') // Переносим HTML в продакшен
    .pipe(gulp.dest('dist'));

});